package ru.tsc.bagrintsev.tm.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IAuthServiceDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserServiceDTO;
import ru.tsc.bagrintsev.tm.dto.model.SessionDTO;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignOutRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserViewProfileRequest;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignInResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignOutResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserViewProfileResponse;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.user.AccessDeniedException;
import ru.tsc.bagrintsev.tm.exception.user.LoginIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;

import java.security.GeneralSecurityException;

@Controller
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(
            @NotNull final IUserServiceDTO userService,
            @NotNull final IAuthServiceDTO authService
    ) {
        super(userService, authService);
    }

    @NotNull
    @Override
    @WebMethod
    public UserSignInResponse signIn(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserSignInRequest request
    ) throws JsonProcessingException, AccessDeniedException, LoginIsIncorrectException, GeneralSecurityException, PasswordIsIncorrectException, UserNotFoundException {
        @NotNull final String token = authService.signIn(request.getLogin(), request.getPassword());
        return new UserSignInResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserSignOutResponse signOut(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserSignOutRequest request
    ) {
        check(request, Role.values());
        return new UserSignOutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserViewProfileResponse viewProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserViewProfileRequest request
    ) {

        @Nullable final SessionDTO session = check(request, Role.values());
        @Nullable final UserDTO user = userService.findOneById(session.getUserId());
        return new UserViewProfileResponse(user);
    }

}
