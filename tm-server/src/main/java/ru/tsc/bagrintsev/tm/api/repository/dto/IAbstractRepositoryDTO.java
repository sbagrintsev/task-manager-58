package ru.tsc.bagrintsev.tm.api.repository.dto;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public interface IAbstractRepositoryDTO<M extends AbstractModelDTO> {

    void add(@NotNull final M record);

    void addAll(@NotNull final Collection<M> records);

    void clearAll();

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull final String id);

    @NotNull
    EntityManager getEntityManager();

    void setEntityManager(@NotNull EntityManager entityManager);

    long totalCount();

}
