package ru.tsc.bagrintsev.tm.configuration;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AvailableSettings;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.EnableAsync;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;
import ru.tsc.bagrintsev.tm.dto.model.SessionDTO;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Session;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.model.User;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@EnableAsync
@Configuration
@ComponentScan("ru.tsc.bagrintsev.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory factory) {
        return factory.createEntityManager();
    }

    @NotNull
    @Bean(destroyMethod = "close")
    public EntityManagerFactory entityManagerFactory(@NotNull final IPropertyService propertyService) {
        @NotNull final Map<String, Object> settings = new HashMap<>();
        settings.put(AvailableSettings.DRIVER, propertyService.getDatabaseDriver());
        settings.put(AvailableSettings.URL, propertyService.getDatabaseUrl());
        settings.put(AvailableSettings.USER, propertyService.getDatabaseUserName());
        settings.put(AvailableSettings.PASS, propertyService.getDatabaseUserPassword());
        settings.put(AvailableSettings.DIALECT, propertyService.getDatabaseSqlDialect());
        settings.put(AvailableSettings.HBM2DDL_AUTO, propertyService.getDatabaseHbm2DdlAuto());
        settings.put(AvailableSettings.C3P0_MAX_SIZE, propertyService.getDatabasePoolMaxSize());
        settings.put(AvailableSettings.C3P0_MIN_SIZE, propertyService.getDatabasePoolMinSize());
        settings.put(AvailableSettings.C3P0_TIMEOUT, propertyService.getDatabasePoolTimeout());
        settings.put(AvailableSettings.SHOW_SQL, propertyService.getDatabaseShowSql());
        settings.put(AvailableSettings.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        settings.put(AvailableSettings.USE_SQL_COMMENTS, propertyService.getDatabaseUseSqlComments());
        settings.put(AvailableSettings.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseUseSecondLevelCache());
        settings.put(AvailableSettings.CACHE_REGION_FACTORY, propertyService.getDatabaseCacheRegionFactoryClass());
        settings.put(AvailableSettings.USE_QUERY_CACHE, propertyService.getDatabaseCacheUseQueryCache());
        settings.put(AvailableSettings.USE_MINIMAL_PUTS, propertyService.getDatabaseCacheUseMinimalPuts());
        settings.put(AvailableSettings.CACHE_REGION_PREFIX, propertyService.getDatabaseCacheRegionPrefix());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(SessionDTO.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    public Properties properties() throws IOException {
        @NotNull final Properties properties = new Properties();
        properties.load(ClassLoader.getSystemResourceAsStream("application.properties"));
        return properties;
    }

}
