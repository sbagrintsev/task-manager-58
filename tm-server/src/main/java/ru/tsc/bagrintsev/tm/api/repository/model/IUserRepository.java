package ru.tsc.bagrintsev.tm.api.repository.model;

import org.apache.ibatis.annotations.Mapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.model.User;

import java.util.Collection;
import java.util.List;

@Mapper
public interface IUserRepository extends IAbstractRepository<User> {

    @Override
    void add(@NotNull final User model);

    @Override
    void addAll(@NotNull final Collection<User> records);

    @Override
    void clearAll();

    @Override
    @Nullable
    List<User> findAll();

    @Nullable
    User findByEmail(@NotNull final String email);

    @Nullable
    User findByLogin(@NotNull final String login);

    @Override
    @Nullable
    User findOneById(@NotNull final String id);

    boolean isEmailExists(@NotNull final String email);

    boolean isLoginExists(@NotNull final String login);

    void removeByLogin(@NotNull final String login);

    void setParameter(@NotNull final User user);

    void setRole(
            @NotNull final String login,
            @NotNull final Role role
    );

    void setUserPassword(
            @NotNull final String login,
            @NotNull final String password,
            final byte @NotNull [] salt
    );

    @Override
    long totalCount();

}
