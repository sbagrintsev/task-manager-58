package ru.tsc.bagrintsev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.repository.dto.IAbstractRepositoryDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IAbstractServiceDTO;
import ru.tsc.bagrintsev.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

@Service
abstract class AbstractServiceDTO<M extends AbstractModelDTO, R extends IAbstractRepositoryDTO<M>> implements IAbstractServiceDTO<M> {

    @NotNull
    public ApplicationContext context;

    @NotNull
    public abstract List<M> findAll();

    @NotNull
    public abstract Collection<M> set(@NotNull final Collection<M> records);

    @Autowired
    void setContext(@NotNull final ApplicationContext context) {
        this.context = context;
    }

    public abstract long totalCount();

}
