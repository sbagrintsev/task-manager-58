package ru.tsc.bagrintsev.tm.repository.dto;

import jakarta.persistence.EntityManager;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.bagrintsev.tm.api.repository.dto.IAbstractRepositoryDTO;
import ru.tsc.bagrintsev.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IAbstractRepositoryDTO<M> {

    @NotNull
    protected final Class<M> clazz;

    @NotNull
    @Getter
    @Setter
    protected EntityManager entityManager;

    public void add(@NotNull final M record) {
        entityManager.persist(record);
    }

    @Override
    public void addAll(@NotNull final Collection<M> records) {
        records.forEach(this::add);
    }

    @Override
    public void clearAll() {
        @NotNull final String jpql = String.format("DELETE FROM %s", clazz.getSimpleName());
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public @Nullable List<M> findAll() {
        @NotNull final String jpql = String.format("FROM %s", clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, clazz)
                .getResultList();
    }

    @Override
    public @Nullable M findOneById(@NotNull String id) {
        return entityManager.find(clazz, id);
    }

    @Override
    public long totalCount() {
        @NotNull final String jpql = String.format("SELECT count(*) FROM %s", clazz.getSimpleName());
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

}
