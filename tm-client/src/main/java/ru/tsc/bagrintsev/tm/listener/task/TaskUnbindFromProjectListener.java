package ru.tsc.bagrintsev.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class TaskUnbindFromProjectListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskUnbindFromProjectListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.TASK_ID);
        @Nullable final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setTaskId(taskId);
        taskEndpoint.unbindTaskFromProject(request);
    }

    @NotNull
    @Override
    public String name() {
        return "task-unbind";
    }

}
