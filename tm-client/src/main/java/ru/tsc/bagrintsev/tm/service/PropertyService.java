package ru.tsc.bagrintsev.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    private final String APPLICATION_VERSION = "buildNumber";

    @NotNull
    private final String AUTHOR_EMAIL = "email";

    @NotNull
    private final String AUTHOR_NAME = "developer";

    @NotNull
    @Value("#{environment['passwordHash.iterations']}")
    private Integer passwordHashIterations;

    @NotNull
    @Value("#{environment['passwordHash.keyLength']}")
    private Integer passwordHashKeyLength;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['server.port']}")
    private String serverPort;

    @NotNull
    @Override
    public String getApplicationVersion() {
        try {
            return Manifests.read(APPLICATION_VERSION);
        } catch (IllegalArgumentException e) {
            return "1.31.test";
        }
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        try {
            return Manifests.read(AUTHOR_EMAIL);
        } catch (IllegalArgumentException e) {
            return "testEmail";
        }
    }

    @NotNull
    @Override
    public String getAuthorName() {
        try {
            return Manifests.read(AUTHOR_NAME);
        } catch (IllegalArgumentException e) {
            return "testName";
        }
    }

}
