package ru.tsc.bagrintsev.tm.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
public class ConsoleEvent {

    @NotNull
    private final String name;

}
