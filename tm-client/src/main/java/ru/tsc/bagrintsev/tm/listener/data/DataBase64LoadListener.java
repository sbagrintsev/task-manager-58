package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataBase64LoadRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class DataBase64LoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    @Override
    public String description() {
        return "Load current application state from base64 file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64LoadListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        domainEndpoint.loadBase64(new DataBase64LoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
