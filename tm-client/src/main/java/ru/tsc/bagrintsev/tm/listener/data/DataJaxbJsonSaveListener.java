package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJaxbJsonSaveRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class DataJaxbJsonSaveListener extends AbstractDataListener {

    @NotNull
    @Override
    public String description() {
        return "Save current application state in json file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJaxbJsonSaveListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        domainEndpoint.saveJaxbJson(new DataJaxbJsonSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-jaxb-json";
    }

}
