package ru.tsc.bagrintsev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.bagrintsev.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.listener.AbstractListener;

import java.util.Collection;

public abstract class AbstractSystemListener extends AbstractListener {

    @Autowired
    @NotNull
    public IPropertyService propertyService;

    @Autowired
    @NotNull
    public ISystemEndpoint systemEndpoint;

    @Autowired
    Collection<AbstractListener> repository;

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

}
