package ru.tsc.bagrintsev.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.system.VersionRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public class VersionListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String description() {
        return "Print version.";
    }

    @Override
    @SneakyThrows
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        System.out.printf("task-manager-client version: %s%n", propertyService.getApplicationVersion());
        System.out.printf("task-manager-server version: %s%n", systemEndpoint.getVersion(new VersionRequest()).getVersion());
    }

    @EventListener(condition = "@versionListener.name() == #consoleEvent.name")
    public void listenName(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @EventListener(condition = "@versionListener.shortName() == #consoleEvent.name")
    public void listenShort(@NotNull ConsoleEvent consoleEvent) {
        handle(consoleEvent);
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String shortName() {
        return "-v";
    }

}
