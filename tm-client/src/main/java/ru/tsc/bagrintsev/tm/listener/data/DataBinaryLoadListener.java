package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataBinaryLoadRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class DataBinaryLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    @Override
    public String description() {
        return "Load current application state from binary file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinaryLoadListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        domainEndpoint.loadBinary(new DataBinaryLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
