package ru.tsc.bagrintsev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.bagrintsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.bagrintsev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.listener.AbstractListener;

public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    public IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    public IUserEndpoint userEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @NotNull
    @Override
    public String shortName() {
        return "";
    }

}
