package ru.tsc.bagrintsev.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskCreateRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class TaskCreateListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskCreateListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.NAME);
        @Nullable final String name = TerminalUtil.nextLine();
        showParameterInfo(EntityField.DESCRIPTION);
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setDescription(description);
        request.setName(name);
        taskEndpoint.createTask(request);
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

}
