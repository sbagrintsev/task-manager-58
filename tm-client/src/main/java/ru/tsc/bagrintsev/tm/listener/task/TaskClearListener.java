package ru.tsc.bagrintsev.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskClearRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public class TaskClearListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskClearListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        taskEndpoint.clearTask(new TaskClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

}
