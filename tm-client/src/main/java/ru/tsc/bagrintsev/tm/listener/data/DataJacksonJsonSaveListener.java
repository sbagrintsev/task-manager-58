package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonJsonSaveRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class DataJacksonJsonSaveListener extends AbstractDataListener {

    @NotNull
    @Override
    public String description() {
        return "Save current application state in json file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJacksonJsonSaveListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        domainEndpoint.saveJacksonJson(new DataJacksonJsonSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-json";
    }

}
