package ru.tsc.bagrintsev.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public final class UserSignInListener extends AbstractUserListener {

    @NotNull
    @Override
    public String description() {
        return "Sign user in.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userSignInListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo(EntityField.PASSWORD);
        @NotNull final String password = TerminalUtil.nextLine();
        @Nullable final String token = authEndpoint.signIn(new UserSignInRequest(login, password)).getToken();
        setToken(token);
        System.out.println(token);
    }

    @NotNull
    @Override
    public String name() {
        return "sign-in";
    }

}
