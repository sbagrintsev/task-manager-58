package ru.tsc.bagrintsev.tm.dto.response.system;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ServerAboutResponse extends AbstractResponse {

    @NotNull
    private String email;

    @NotNull
    private String name;

}
