package ru.tsc.bagrintsev.tm.api.model;

import org.jetbrains.annotations.NotNull;

public interface ICommand {

    @NotNull
    String description();

    @NotNull
    String name();

    @NotNull
    String shortName();

}
