package ru.tsc.bagrintsev.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserViewProfileResponse extends AbstractResultResponse {

    @Nullable
    private UserDTO user;

    public UserViewProfileResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

    public UserViewProfileResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
